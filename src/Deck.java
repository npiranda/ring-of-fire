import java.util.ArrayList;

public class Deck {
    private final ArrayList<Carte> deck;

    public Deck(ArrayList<Carte> cards){
        this.deck = cards;
    }

    public void nameCards() {
        int iteratorFamily = 0;
        int iteratorValue = 0;
        String[] families = {"Coeur", "Carreau", "Pique", "Trèfle"};
        String[] values = {"Deux", "Trois", "Quatre", "Cinq", "Six",
                "Sept","Huit","Neuf","Dix","Valet","Dame","Roi","As"};
        for (Carte card: deck) {
            card.setName(values[iteratorValue] + " de " + families[iteratorFamily]);
            if (iteratorValue < 12) {
                iteratorValue++;
            } else {
                iteratorValue = 0;
                iteratorFamily = (iteratorFamily + 1 ) % 4;
            }

        }
    }

    public String pickCard(ArrayList<Player> players){
        String card;
        String rule;
        int random = (int) (Math.random() * (51));
        while(deck.get(random).isDrawed()){
            random = (int) (Math.random() * (51));
        }
        deck.get(random).setDrawed(true);
        card = deck.get(random).getName();
        rule = getRuleByCard(random,players);
        return card + "\n" + rule;
    }

    public String getRuleByCard(int card, ArrayList<Player> players){
        int drawedCard = ((card%13)+2);
        String effect = "";

            if (card <= 26){
                effect = "donnes";
            } else {
                effect = "prends";
            }

        String result = "";
        if (drawedCard <= 6){
                result = "Tu "+effect+" " +drawedCard+" gorgées.";
        }
        if (drawedCard == 14){
            result = "Tu "+effect+" cul sec !";
        }
        if (drawedCard == 7){
            result = "Invente une nouvelle règle !";
        }
        if (drawedCard == 8){
            int randomShots = (int)(Math.random() * ((7 - 2) + 1)) + 2;
            result = "Tu " + effect + " " + randomShots + " gorgées.";
        }
        if (drawedCard == 9){
            result = "Jeu de la valise !";
        }
        if (drawedCard == 10){
            int cupidon = (int) (Math.random() * players.size());
            result = "Tu boiras tes trois prochains coups avec " + players.get(cupidon).getName();
        }
        if (drawedCard == 11){
            result = "Donne un thème !";
        }
        if (drawedCard == 12){
            result = "Ni oui ni non !";
        }
        if (drawedCard == 13){
            result = "C'est toi le roi ! Tu as le droit de doubler les gorgées d'un joueur deux fois !";
        }
        return result;
    }



    
}
