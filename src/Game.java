import java.util.ArrayList;
import java.util.Scanner;

public class Game {

    public static void main(String[] args) {
        int nbPlayers;
        String playerName;
        ArrayList<Player> players = new ArrayList<>();
        ArrayList<Carte> cards = new ArrayList<>();



        Scanner scanner = new Scanner(System.in);
        System.out.print("Nombre de joueurs : ");
        nbPlayers = Integer.parseInt(scanner.nextLine());

        for (int i = 1; i < nbPlayers+1 ; i++){
            System.out.print("Joueur "+i+" :");
            playerName = scanner.nextLine();
            Player player = new Player(playerName);
            players.add(player);
        }

        for (int nbCartes = 0 ; nbCartes < 52 ; nbCartes++){
            Carte carte = new Carte(false,"");
            cards.add(carte);
        }

        Deck deck = new Deck(cards);
        deck.nameCards();

        int iteratorPlayer = 0;
        while(true){
            System.out.print(players.get(iteratorPlayer).getName()+ " pioche un " + deck.pickCard(players));
            scanner.nextLine();
            iteratorPlayer = (iteratorPlayer + 1) % (nbPlayers);

        }

    }
}
