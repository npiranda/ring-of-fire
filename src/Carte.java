public class Carte {
    private boolean drawed;
    private String name;

    public Carte(boolean drawed, String name){
        this.drawed = drawed;
        this.name = name;
    }



    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isDrawed() {
        return drawed;
    }

    public void setDrawed(boolean drawed) {
        this.drawed = drawed;
    }
}
